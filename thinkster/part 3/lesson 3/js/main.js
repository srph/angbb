var myApp = angular.module('myApp', []);

myApp.factory('Avengers', function() {
	var Avengers = {};
	Avengers.cast = [
		{
			name: "Robert Downey Jr.",
			character: "Iron Man"
		},

		{
			name: "Kier Borromeo",
			character: "Thor"
		}
	];

	return Avengers;
});

function AvengersCtrl($scope, Avengers) {
	$scope.avengers = Avengers;
}