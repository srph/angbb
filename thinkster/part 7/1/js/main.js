var app = angular.module("app", []);

app.directive("dumbPassword", function() {
	var validElement = angular.element('<div> {{ model.input }} </div>');

	var link = function(scope) {
		scope.$watch("model.input", function(value) {
			if(value === "password") {
				validElement.toggleClass('alert alert-danger');
			}
		});
	};

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'dumb.html',
		compile: function(parent) {
			parent.append(validElement);

			return link;
		}
	};
});