var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "ViewCtrl",
			resolve: {
				loadData: viewCtrl.loadData,
			}
		});
});

app.controller("appCtrl", function($rootScope) {
	$rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
		console.log(previous);
	});
});

var viewCtrl = app.controller("ViewCtrl", function($scope) {
	$scope.model = {
		message: "Amigo yolo!"
	};
});

viewCtrl.loadData = function($q, $timeout) {
	var defer = $q.defer();
	$timeout(function() {
		defer.reject("Cr");
	}, 2000);

	return defer.promise;
}