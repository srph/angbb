var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "AppCtrl",
			resolve: {
				app: function($q, $timeout) {
					var deffered = $q.defer();
					$timeout(function() {
						deffered.resolve();
					}, 1000)
					return deffered.promise;
				}
			}
		});
});

app.controller("AppCtrl", function($scope) {
	$scope.model = {
		message: "App amigo!"
	}
});