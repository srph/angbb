var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when("/:country/:state/:city",
		{
			templateUrl: "app.html",
			controller: "AppCtrl"
		});
});

app.controller("AppCtrl", function($scope, $routeParams) {
	$scope.model = {
		message: "Address: " +
					$routeParams.country + ", " +
					$routeParams.state + ", " +
					$routeParams.city
	}
});