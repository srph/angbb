var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "AppCtrl"
		});
});

app.controller("AppCtrl", function($scope, $q) {
	var defer =  $q.defer();

	defer.promise
		.then(function(weapon) {
			alert('Swaggity yolow' + weapon);
			return "swag";
		})
		.then(function(yolow) {
			alert('Yolo more' + yolow);
		});

	defer.resolve("yolow");

	$scope.model = {
		message: "App amigo!"
	}
});