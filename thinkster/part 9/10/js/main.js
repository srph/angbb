var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
	$templateCache.put("error.html", "<div class='alert alert-danger' ng-show='isError'>Error!</div>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "ViewCtrl",
			resolve: {
				loadData: viewCtrl.loadData,
			}
		});
});

app.directive("error", function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: "error.html",
		link: function(scope) {
			$rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
				scope.isError = true;
			});
		}
	};
});

app.controller("appCtrl", function($rootScope) {
	$rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
		console.log(event);
	});
});

var viewCtrl = app.controller("ViewCtrl", function($scope) {
	$scope.model = {
		message: "Amigo yolo!"
	};
});

viewCtrl.loadData = function($q, $timeout) {
	var defer = $q.defer();
	$timeout(function() {
		defer.reject("Cr");
	}, 2000);

	return defer.promise;
}