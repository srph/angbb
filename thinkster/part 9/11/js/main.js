var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> <a ng-click='changeRoute()'> Change routes </a> </h1>");
	$templateCache.put("new.html", "<h1> New here, Amigo! </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "ViewCtrl",
			resolve: {
				loadData: viewCtrl.loadData,
			}
		})
		.when("/new",
		{
			templateUrl: "new.html",
			controller: "NewCtrl",
			resolve: {
				loadData: viewCtrl.loadData
			}
		})
});

app.controller("appCtrl", function($scope, $rootScope, $route, $location) {
	$rootScope.$on("$routeChangeStart", function(event, current, previous, rejection) {
		console.log($scope, $rootScope, $route, $location)
	});
	$rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
		console.log($scope, $rootScope, $route, $location);
	});
});

var viewCtrl = app.controller("ViewCtrl", function($scope, $route, $location) {
	$scope.changeRoute = function() {
		console.log($scope);
		$location.path("/new");
	}
});

app.controller("NewCtrl", function($scope, loadData, $template) {
	console.log($scope, loadData, $template);
});

viewCtrl.loadData = function($q, $timeout) {
	var defer = $q.defer();
	$timeout(function() {
		defer.resolve({ message: "success" });
	}, 2000);

	return defer.promise;
}