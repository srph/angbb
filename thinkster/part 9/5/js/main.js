var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "AppCtrl"
		})
		.when("/pizza/:crust",
		{
			redirectTo: function(routeParams, path, search) {
				console.log(routeParams);
				console.log(path);
				console.log(search);
				return "/" + routeParams.crust;
			}
		})
		.when("/deep",
		{
			template: 'Deep dish'
		})
		.otherwise({
			redirectTo: "/"
		})
});

app.controller("AppCtrl", function($scope) {
	$scope.model = {
		message: "App amigo!"
	}
});