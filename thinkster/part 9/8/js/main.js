var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	$routeProvider
		.when('/',
		{
			templateUrl: "app.html",
			controller: "appCtrl",
			resolve: {
				loadData: appCtrl.loadData,
				prepData: appCtrl.prepData
			}
		});
});

var appCtrl = app.controller("appCtrl", function($scope, $route) {
	console.log($route);
	$scope.model = {
		message: "App amigo!"
	}
});

appCtrl.loadData = function($q, $timeout) {
	var defer = $q.defer();
	$timeout(function() {
		defer.resolve("Yolowing");
	}, 2000);

	return defer.promise;
};

appCtrl.prepData = function($q, $timeout) {
	var defer = $q.defer();
	$timeout(function() {
		defer.resolve("Swagg");
	}, 2000);

	return defer.promise;
}