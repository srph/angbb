var app = angular.module("app", ["ngRoute"]);

app.run(function($templateCache) {
	$templateCache.put("app.html", "<h1> {{ model.message }} </h1>");
});

app.config(function($routeProvider) {
	// $routeProvider.when("/",
	// 	{
	// 		templateUrl: "app.html",
	// 		controller: "AppCtrl"
	// 	}
	// );
});

app.controller("AppCtrl", function($scope, $route) {
	$route.routes["/"] = {
		templateUrl: "app.html",
		controller: "AppCtrl"
	}
	$scope.model = {
		message: "Sup swaggity yolo"
	};
});
