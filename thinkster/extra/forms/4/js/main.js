angular.module('myApp', []).directive('contenteditable', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			// view -> model
			elm.on('keyup', function() {
				scope.$apply(function() {
					ctrl.$setViewValue(elm.html());
				});
			});

			// model -> view (re-render)
			ctrl.$render = function() {
				elm.html(ctrl.$viewValue);
			};

			// load initialization from dom
			ctrl.$setViewValue(elm.html());
		}
	};
});