angular.module('FilterInControllerModule', [])
	.controller('FilterController', ['filterFilter', function(filterFilter) {
		this.array = [
			{ name: 'Kier' },
			{ name: 'Jealian' },
			{ name: 'Brian' },
			{ name: 'Ivan' },
			{ name: 'Pogi' },
		];

		this.filteredArray = filterFilter(this.array, 'a');
	}]);