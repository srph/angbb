var app = angular.module("myApp", []);

app.filter("greet", function() {
	return function(name) {
		return 'Hello ' + name + '!';
	}
});