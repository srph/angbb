angular.module("myApp.service", [])
	.value("greeter", {
		salutation: "Hello!",

		localize: function(localization) {
			this.salutation = localization.salutation;
		},

		greet: function(name) {
			return this.salutation + ' ' + name;
		}
	})
	.value("user", {
		load: function(name) {
			this.name = name;
		}
	});

angular.module("myApp.directive", []);
angular.module("myApp.filter", []);

angular.module("myApp", ["myApp.service", "myApp.directive", "myApp.filter"])
	.run(function(greeter, user) {
		greeter.localize({
			salutation: 'Bonjour'
		});

		user.load('World');
	});

var Ctrl = function($scope, greeter, user) {
	$scope.greeting = greeter.greet(user.name);
}