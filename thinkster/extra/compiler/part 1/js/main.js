var app = angular.module('drag', []);

app.directive('draggable', function($document) {
	return function(scope, element, attr) {

		// Initialize variables
		var startX = 0,
			startY = 0,
			x = 0,
			y = 0;

		// Make the element position to relative
		element.css({
			position: 'relative',
			cursor: 'pointer'
		});

		// On mouse down
		element.on('mousedown', function(e) {
			e.preventDefault();
			// Change position:
			startX = e.screenX - x;
			startY = e.screenY - y;

			// On mouse move while held down
			$document.on('mousemove', mousemove);
			// On mouse up
			$document.on('mouseup', mouseup);
		});

		function mousemove(e) {
			x = e.screenX - startX;
			y = e.screenY - startY;

			// Move element position (relative)
			element.css({
				top: y + 'px',
				left: x + 'px'
			});
		}

		function mouseup() {
			$document.unbind('mousemove', mousemove);
			$document.unbind('mouseup', mouseup);
		}

	}
});