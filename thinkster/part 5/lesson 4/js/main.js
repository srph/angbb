var app = angular.module('phoneApp', [])
	// create a controller called AppController
app.controller('AppCtrl', function($scope) {

	// create a method called callHome that accepts 1 parameter
		$scope.callHome = function(message) {
			// alert the given parameter
			alert(message);
		}
	
	});

	// create a phone directive
app.directive('phone', function() {
	// return:
	return {
		// a dial with &
		scope: {
			dial: '&'
		},
	
		// button template
		template: '<input type="text" class="form-control" ng-model="value">' +
			'<div class="btn btn-primary" ng-click="dial({ message: value })">' +
			'Call home!</div>'
	}
});